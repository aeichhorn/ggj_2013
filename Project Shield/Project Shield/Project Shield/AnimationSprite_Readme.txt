How to use the classes MoblileSprites.cs, FrameAnimation.cs and AnimatedSprite.cs:

Example: 

Declaration:

    private Texture2D raven; //The SpriteSheet
    public MobileSprites Raven;

LoadContent Method: 

    //Load Animted Sprite as MobileSprite	
    Raven = new MobileSprites(raven);

    //Set Animations - Parameters: (string AnimationName, int 1stFrameX, int FirstFrameY, int FrameWidth, int FrameHeight, int AmountFrames, float FrameLength)
    Raven.Sprite.AddAnimation("sitting", 0, 0, 96, 118, 1, 0.1f);

    //		     - oter Parameter: (string AnimationName, int 1stFrameX, int 1stFrameY, int FrameWidth, int FrameHeight, int AmountFrames,
           float FrameLength, string NextAnimation)
    Raven.Sprite.AddAnimation("flying", 96, 0, 148, 177, 9, 0.2f, "sitting");

    //Set current Animation
    Raven.Sprite.CurrentAnimation = "flying";

    //Set Position
    Raven.Position = new Vector2(1000, -180);
     
    //Animated Sprite can move to special target
    Raven.Target = new Vector2(1472, 400);

    //Movement Speed
    Raven.Speed = 3;

    //Loop Path
    Raven.LoopPath = false;

    //Move on a special path?
    Raven.IsPathing = false;

    //If true, Sprite moves to target
    Raven.IsMoving = false;

Update Method: 

    //Update movement
    Raven.Update(gameTime);

Draw Method: 

    //Draw AnimatedSprite
    Raven.Draw(spriteBatch);
    
