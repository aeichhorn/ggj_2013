﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Project_Shield.Logic
{
    public class FirstPathStep : PathStep
    {
        private readonly IList<Match> m_matches = new List<Match>();

        public IEnumerable<Match> Matches 
        { 
            get 
            { 
                if(m_matches.Count > 0)
                {
                    var matches = m_matches.OrderBy(match => match.ComponentType.Priority);
                    var filterComponent = matches.First().ComponentType;
                    return matches.Where(match => match.ComponentType == filterComponent); 
                }
                return m_matches;
            }
        }

        public FirstPathStep(Wound node)
            : base(node, 0, null, null)
        {
        }

        protected override void CreateMatch(IList<Wound> matchedWounds, ComponentType bestMatch)
        {
            matchedWounds.Add(StepFrom);
            m_matches.Add(new Match(bestMatch, matchedWounds));
        }

        public override void MatchImmediateNeighbors()
        {
            if(m_nextSteps.Count > 1)
            {
                IList<IList<Wound>> pathes = new List<IList<Wound>>();
                for(int i = 0; i < m_nextSteps.Count; ++i)
                {
                    for(int j = i + 1; j < m_nextSteps.Count; ++j)
                    {
                        var wounds = new List<Wound>();
                        wounds.Add(m_nextSteps[i].StepFrom);
                        wounds.Add(m_nextSteps[j].StepFrom);
                        pathes.Add(wounds);
                    }
                }

                foreach (var path in pathes)
                {
                    ComponentType bestMatch = new ChainStep(path.Select(wound => wound.AppliedComponentType)).BestMatch;
                    if (bestMatch != null)
                    {
                        CreateMatch(path, bestMatch);
                    }
                }
            }
            
        }
    }
}
