﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Project_Shield.Logic
{
    public class ComponentType 
    {
        public String Name { get { return Type.ToString(); } }
        public Type Type { get; private set; }
        public int Priority { get; private set; }

        public ComponentType(Type type, int priority = 10)
        {
            Type = type;
            Priority = priority;
        }
    }
}
