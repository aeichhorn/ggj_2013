﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Project_Shield.Logic
{
    public class Match
    {
        public ComponentType ComponentType { get; private set; }
        public IEnumerable<Wound> Wounds { get; private set; }

        public Match(ComponentType componentType, IEnumerable<Wound> wounds)
        {
            ComponentType = componentType;
            Wounds = wounds;
        }
    }
}
