﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Project_Shield.Logic
{
    public class Board
    {
        const int DEFAULT_NO_OF_NEIGHBORS = 8;
        private readonly Wound[,] m_wounds;
        private Pathes m_pathes;
        private int m_healedFieldsNeededToWin;
        //private int m_scoreNeededToWin = 1500;

        public int Width { get; private set; }
        public int Height { get; private set; }

        public bool GameWon { get; private set; }
        public bool GameOver { get; private set; }

        public int Score { get; private set;}
 
        public Board(Wound[,] wounds)
        {
            m_wounds = wounds;
            Width = m_wounds.GetUpperBound(0);
            Height = m_wounds.GetUpperBound(1);
            float temp = Width * Height;
            m_healedFieldsNeededToWin = (int)((temp / 100.0f) * 70.0f);
            //CheckWoundNeighborhood();
        }

        public void Update()
        {
            int healedFieldCount = 0;
            ForEachWound((x, y) => 
            {
                var wound = m_wounds[x, y];
                wound.Update();
                if (wound.HealthStatus == HealthStatus.SafeAndSound)
                {
                    healedFieldCount++;
                }
                else if (wound.AppliedComponentType == ComponentTypes.Leucocyte
                    || wound.AppliedComponentType == ComponentTypes.Cell)
                {
                    EliminateVirus(wound);
                }
                return true; // continue loop
            });
            if (healedFieldCount >= m_healedFieldsNeededToWin)
            {
                GameWon = true;
            }
            else if (healedFieldCount == 0)
            {
                GameOver = true;
            }

        }

        public void AddScore(int score)
        {
            Score += score;
			//if(Score >= m_scoreNeededToWin)
			//{
			//    GameWon = true;
			//}
        }

        public Wound WoundAt(int x, int y)
        {
            return m_wounds[x, y];
        }

        public IEnumerable<Match> ApplyComponent(int x, int y, IToken entity)
        {
            Wound wound = m_wounds[x,y];
            bool shouldFindMatch = wound.ApplyComponent(entity);
 
            var compositeComponent = entity.ComponentType as CompositeComponentType;
            if (compositeComponent != null)
            {
                AddScore(compositeComponent.Score);
            }
            else if (entity.ComponentType == ComponentTypes.Phagocyte)
            {
                AddScore(5);
            }
            if(shouldFindMatch)
            {
                return FindMatchesFor(wound);
            }
            return new List<Match>();
        }

        private IEnumerable<Match> FindMatchesFor(Wound wound)
        {
            Wound[] neighbors = new Wound[4];

            m_pathes = new Pathes(wound);

            while (m_pathes.HasOpenPath)
            {
                PathStep step = m_pathes.CurrentPathStep;
                FindNeighborNodes(step.StepFrom.X, step.StepFrom.Y, neighbors);
                foreach (var neighbor in neighbors)
                {
                    step.Add(neighbor);
                }
                step.MatchImmediateNeighbors();
                m_pathes.MoveNext();
            }
            return m_pathes.Matches;
        }

        private void FindNeighborNodes(int x, int y, Wound[] inNeighbors)
        {           
            if (y > 0)
                inNeighbors[0] = m_wounds[x, y - 1];
            else
                inNeighbors[0] = null;
            
            if (x > 0)
                inNeighbors[1] = m_wounds[x - 1, y];
            else
                inNeighbors[1] = null;

            if (x < Width)
                inNeighbors[2] = m_wounds[x + 1, y];
            else
                inNeighbors[2] = null;

            if (y < Height )
                inNeighbors[3] = m_wounds[x, y + 1];
            else
                inNeighbors[3] = null;
            
            if (inNeighbors.Length  == 8)
            {
                if ((x > 0) && (y > 0) )
                    inNeighbors[4] = m_wounds[x - 1, y - 1];
                else
                    inNeighbors[4] = null;

                if ((x < Width ) && (y > 0))
                    inNeighbors[5] = m_wounds[x + 1, y - 1];
                else
                    inNeighbors[5] = null;

                if ((x > 0) && (y < Height ))
                    inNeighbors[6] = m_wounds[x - 1, y + 1];
                else
                    inNeighbors[6] = null;

                if ((x < Width ) && (y < Height ) )
                    inNeighbors[7] = m_wounds[x + 1, y + 1];
                else
                    inNeighbors[7] = null;
            }
        }

        private void ForEachWound(Func<int, int, bool> func, int xOffset = 0, int yOffset = 0)
        {
            for (int x = xOffset; x <= Width; ++x)
            {
                for (int y = yOffset; y <= Height; ++y)
                {
                    var doContinue = func(x, y);
                    if (!doContinue)
                    {
                        return;
                    }
                }
            }
            if (xOffset > 0 || yOffset > 0)
            {
                ForEachWound((x, y) =>
                {
                    if (x == xOffset && y == yOffset)
                    {
                        return false;
                    }
                    return func(x, y);
                });
            }
        }

        private IList<TileDirections> m_directions = new List<TileDirections>
        {
            TileDirections.South,
            TileDirections.East,
            TileDirections.West,
            TileDirections.North,
            TileDirections.SouthEast,
            TileDirections.SouthWest,
            TileDirections.NorthEast,
            TileDirections.NorthWest
        };

        private void CheckWoundNeighborhood()
        {
            
            Wound[] neighbors = new Wound[DEFAULT_NO_OF_NEIGHBORS];
            ForEachWound((x, y) =>
            {
                Wound wound = m_wounds[x, y];
                if (wound.HealthStatus > HealthStatus.SafeAndSound)
                {
                    wound.NeighborState = 255;
                    FindNeighborNodes(x, y, neighbors);
                    TileDirections neighborState = 0;
                    for (int i = 0; i < DEFAULT_NO_OF_NEIGHBORS; ++i)
                    {
                        if (neighbors[i] != null && neighbors[i].HealthStatus == HealthStatus.SafeAndSound)
                        {
                            neighborState = (TileDirections)neighbors[i].NeighborState;
                            neighbors[i].NeighborState = 0;// (int)neighborState.Add(m_directions[i]);
                            //neighborState = neighborState.Add(m_directions[i]);
                        }
                    }
                }
                return true; // continue loop
                //m_wounds[x, y].NeighborState = (int)neighborState;
            });           
        }

        public Wound SpawnVirus(IToken token)
        {
            Wound wound = null;
            var random = new Random();
            int xOffSet = random.Next(0, Width + 1);
            int yOffSet = random.Next(0, Height + 1);
            ForEachWound((x, y) =>
                {
                    Wound potentialWoundForVirus = m_wounds[x, y];
                    if (potentialWoundForVirus.AppliedComponentType != ComponentTypes.Virus
                        && NoLeucocyteAround(potentialWoundForVirus))
                    {
                        wound = potentialWoundForVirus;
                        wound.ApplyComponent(token);
                        return false; // stop loop
                    }
                    return true; // continue loop
                }, xOffSet, yOffSet);
            return wound;
        }

        private bool NoLeucocyteAround(Wound wound)
        {
            if (wound.AppliedComponentType == ComponentTypes.Leucocyte)
            {
                return false;
            }
            Wound[] neighbors = new Wound[DEFAULT_NO_OF_NEIGHBORS];
            FindNeighborNodes(wound.X, wound.Y, neighbors);
            foreach (var neighbor in neighbors)
            {
                if (neighbor != null && neighbor.AppliedComponentType == ComponentTypes.Leucocyte)
                {
                    return false;
                }
            }
            return true;
        }

        public void EliminateVirus(Wound wound)
        {
            Wound[] neighbors = new Wound[DEFAULT_NO_OF_NEIGHBORS];

            FindNeighborNodes(wound.X, wound.Y, neighbors);
            foreach (var neighbor in neighbors)
            {
                if (neighbor != null && neighbor.AppliedComponentType == ComponentTypes.Virus)
                {
                    //AddScore(75);
                    neighbor.Entity.EatenByHungryFellow = true;
                    neighbor.Reset();
                }
            }
        }

    }
}
