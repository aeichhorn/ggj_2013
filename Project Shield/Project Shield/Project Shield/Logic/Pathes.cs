﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Project_Shield.Logic
{
    public class Pathes
    {
        private PathStep m_currentPath;
        private readonly FirstPathStep m_startPathStep;
        
        public bool HasOpenPath { get {return m_startPathStep.HasOpenPath; }}

        public PathStep CurrentPathStep { get { return m_currentPath; } }

        public IEnumerable<Match> Matches { get { return m_startPathStep.Matches; } }
                
        public Pathes(Wound startNode)
        {
            m_startPathStep = new FirstPathStep(startNode);
            m_currentPath = m_startPathStep;
        }

        public bool MoveNext()
        {
            if (m_currentPath != null)
            {
                m_currentPath = m_currentPath.Next;
                return true;
            }
            return false;
        }  
    }   
}
