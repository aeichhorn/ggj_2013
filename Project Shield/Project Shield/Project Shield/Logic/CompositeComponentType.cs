﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Project_Shield.Logic
{
    public class CompositeComponentType : ComponentType
    {
        private readonly ComponentTypeContainer container = new ComponentTypeContainer();

        public int Score { get; private set; }

        public CompositeComponentType(Type type, int score, int priority = 5)
            : base(type, priority)
        {
            Score = score;
        }

        public CompositeComponentType Add(ComponentType componentType)
        {
            container.Add(componentType);
            return this;
        }

        public bool Matches(IDictionary<ComponentType, int> subComponents)
        {
            bool match = false;
            foreach (var element in subComponents)
            {
                int componentCount;
                if (container.ComponentTypes.TryGetValue(element.Key, out componentCount))
                {
                    match = element.Value <= componentCount;
                }
                else
                {
                    match = false;
                }

                if (match == false)
                {
                    break;
                }
            }
            return match;
        }
    }
}
