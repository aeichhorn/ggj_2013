﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Project_Shield.Logic
{
    public class PathStep
    {
        private readonly int m_hierachy;
        protected readonly List<PathStep> m_nextSteps = new List<PathStep>();
        private readonly IChainStep m_chainStep;

        private PathStep m_previousStep;
        private int m_nextStep = 0;
        private bool m_closed;        

        public Wound StepFrom { get; private set; }

        protected PathStep(Wound node, int hierachy, PathStep previous, IChainStep chainStep)
        {
            m_previousStep = previous;
            m_hierachy = hierachy;
            StepFrom = node;
            m_chainStep = chainStep ?? new ChainStep(StepFrom.AppliedComponentType);

            if (LastInHierachy)
            {
                Close();
                m_previousStep.CreateMatch(new List<Wound> { StepFrom }, m_chainStep.BestMatch);
            }
        }

        public virtual bool Add(Wound wound)
        {
            var chainStep = RetrieveNextChainStep(wound);
            if (chainStep != ChainStep.None && !m_closed)
            {
                m_nextSteps.Add(new PathStep(wound, m_hierachy + 1, this, chainStep));
                return true;
            }
            return false;
        }

        public PathStep Next
        {
            get
            {
                while (m_nextStep < m_nextSteps.Count)
                {
                    var nextStep = m_nextSteps[m_nextStep];
                    m_nextStep++;
                    if (!nextStep.m_closed)
                    {
                        return nextStep;
                    }
                }
                if (!HaveChildrenOpenPath)
                {
                    Close();
                    if (m_previousStep != null)
                    {
                        return m_previousStep.Next;
                    }
                    return null;

                }
                return m_previousStep;
            }
        }

        public bool HasOpenPath
        {
            get
            {
                var result = HaveChildrenOpenPath;
                if (result == false && m_nextSteps.Count == 0)
                {
                    result = true;
                }
                return result && !m_closed;
            }
        }

        public override String ToString()
        {
            return string.Format("PathStep at {0}/{1} with ComponentType {2} is closed={3}",
                StepFrom.X, StepFrom.Y, StepFrom.NeededComponentType.Name, m_closed);
        }

        private bool LastInHierachy { get { return m_hierachy == 2; } }

        private void Close()
        {
            m_closed = true;
        }

        private IChainStep RetrieveNextChainStep(Wound wound)
        {
            if (m_chainStep == ChainStep.None
                || wound == null
                || wound.AppliedComponentType == null
                || (m_previousStep != null && wound == m_previousStep.StepFrom))
            {
                return ChainStep.None;
            }
            return m_chainStep.Apply(wound.AppliedComponentType);
        }

        private bool HaveChildrenOpenPath
        {
            get
            {
                foreach (var pathStep in m_nextSteps)
                {
                    if (pathStep.HasOpenPath)
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        protected virtual void CreateMatch(IList<Wound> matchedWounds, ComponentType bestMatch)
        {
            matchedWounds.Add(StepFrom);
            m_previousStep.CreateMatch(matchedWounds, bestMatch);
        }

        /// <summary>
        /// only needed in FirstPathStep
        /// </summary>
        public virtual void MatchImmediateNeighbors()
        {
        }
    }
}
