﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Project_Shield.Logic
{
    public static class BoardFactory
    {

        public static Board CreateEmpty(int width, int height)
        {
            Wound[,] grid = new Wound[width, height];

            for (int x = 0; x < width; ++x)
            {
                for (int y = 0; y < height; ++y)
                {
                    grid[x, y] = new Wound(x, y);
                }
            }

            return new Board(grid);
        }

        public static Board CreateWitHealthStatus(int width, int height)
        {
            Wound[,] grid = new Wound[width, height];
            var random = new Random();
            var maxInitialHealedFields = 4;
            var healedFieldCount = 0;
            for (int x = 0; x < width; ++x)
            {
                for (int y = 0; y < height; ++y)
                {
                    HealthStatus healthStatus = (HealthStatus)random.Next(0, 3);
                    if (healedFieldCount >= maxInitialHealedFields)
                    {
                        healthStatus = HealthStatus.Injured;
                    }
                    if (healthStatus == HealthStatus.SafeAndSound)
                    {
                        healedFieldCount++;
                    }
                    grid[x, y] = new Wound(x, y, healthStatus);
                }
            }

            return new Board(grid);
        }

        public static Board CreateDummy(int width, int height)
        {
            Wound[,] grid = new Wound[width, height];
            var random = new Random();

            for (int x = 0; x < width; ++x)
            {
                for (int y = 0; y < height; ++y)
                {
                    HealthStatus status = HealthStatus.SafeAndSound;
                    if ((x == 0 && y == 1) || (x == 1 & y == 0) || (x == 1 && y == 1))
                    {
                        status = HealthStatus.Injured;
                    }   
                    grid[x, y] = new Wound(x, y, status);
                }
            }

            return new Board(grid);
        }
    }
}
