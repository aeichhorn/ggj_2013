﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Project_Shield.Logic
{
    public interface IToken
    {
        ComponentType ComponentType { get; }
        bool CanBeRemoved { get; }
        bool EatenByHungryFellow { get; set; }
    }
}
