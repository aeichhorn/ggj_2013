﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Project_Shield.Logic
{
    public enum Type
    {
        None, Virus, Sugar, Lipide, Oxygen, Iron, Proteine, Blood, Phagocyte, Thrombocyte, Leucocyte, Cell
    }

    public class ComponentTypes
    {
        public static ComponentType Virus = new ComponentType(Type.Virus);

        public static ComponentType Sugar = new ComponentType(Type.Sugar);
        public static ComponentType Lipid = new ComponentType(Type.Lipide);
        public static ComponentType Oxygen = new ComponentType(Type.Oxygen);
        public static ComponentType Iron = new ComponentType(Type.Iron);
        public static ComponentType Proteine = new ComponentType(Type.Proteine);
        public static ComponentType Blood = new ComponentType(Type.Blood);
        public static ComponentType Phagocyte = new ComponentType(Type.Phagocyte);

        public static CompositeComponentType Thrombocyte = new CompositeComponentType(Type.Thrombocyte, 50)
            .Add(Blood).Add(Iron).Add(Oxygen);

        public static CompositeComponentType Leucocyte = new CompositeComponentType(Type.Leucocyte, 50)
            .Add(Sugar).Add(Lipid).Add(Proteine);

        public static CompositeComponentType Cell = new CompositeComponentType(Type.Cell, 150, 1) 
            .Add(Lipid).Add(Thrombocyte).Add(Proteine);

        public static IEnumerable<CompositeComponentType> AllComposites
            = new List<CompositeComponentType> { Thrombocyte, Leucocyte, Cell };

        public static IList<ComponentType> AllBasicComponents
            = new List<ComponentType> { Virus, Sugar, Lipid, Oxygen, Iron, Proteine, Blood, Phagocyte};

    }
}
