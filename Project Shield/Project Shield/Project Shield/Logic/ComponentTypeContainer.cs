﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Project_Shield.Logic
{
    public class ComponentTypeContainer
    {
        private readonly IDictionary<ComponentType, int> m_componentTypes;

        public IDictionary<ComponentType, int> ComponentTypes { get {return m_componentTypes; }}

        public ComponentTypeContainer()
        {
            m_componentTypes = new Dictionary<ComponentType, int>();
        }

        public ComponentTypeContainer(IDictionary<ComponentType, int> componentTypes)
        {
            m_componentTypes = componentTypes;
        }

        public void Add(ComponentType componentType)
        {
            if (ComponentTypes.ContainsKey(componentType))
            {
                var count = ComponentTypes[componentType];
                ComponentTypes[componentType] = ++count;
            }
            else
            {
                ComponentTypes.Add(componentType, 1);
            }
        }
    }
}
