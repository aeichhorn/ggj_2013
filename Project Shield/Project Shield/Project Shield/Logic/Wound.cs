﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Project_Shield.Logic
{
    public enum HealthStatus
    {
        SafeAndSound,
        Injured,
        SevereInjuries
    }

    public class Wound
    {
 
        public Int32 X { get; private set; }
        public Int32 Y { get; private set; }

        public IToken Entity { get; private set; }
        public ComponentType NeededComponentType { get; private set; }
        public ComponentType AppliedComponentType { get; private set; }  

        public HealthStatus HealthStatus { get; private set; }
        public int NeighborState { get; set; }

        public Wound(int x, int y, HealthStatus health = HealthStatus.SafeAndSound)
        {
            X = x;
            Y = y;
            HealthStatus = health;
        }

        public Wound(int x, int y, ComponentType neededComponentType)
            :this(x, y)
        {
            NeededComponentType = neededComponentType;
        }

        public bool ApplyComponent(IToken token)
        {
            if (token.ComponentType == ComponentTypes.Virus)
            {
                if (AppliedComponentType != null)
                {
                    Entity.EatenByHungryFellow = true;
                    Reset();
                }
                HealthStatus = HealthStatus.Injured;
            }
            if (token.ComponentType == ComponentTypes.Phagocyte 
                && Entity != null
                && Entity.ComponentType != ComponentTypes.Virus)
            {
                Entity.EatenByHungryFellow = true;
                token.EatenByHungryFellow = true;
                Reset();
                return false;
            }
            else if(token.ComponentType == ComponentTypes.Cell)
            {
                HealthStatus = HealthStatus.SafeAndSound;
                /*if (HealthStatus > 0)
                {
                    HealthStatus -= 1;
                }*/
            }
            else // this is old and unused functionality, I still like it to be in here as a reminder
            {
                Entity = token;
                AppliedComponentType = token.ComponentType;
                if (AppliedComponentType == NeededComponentType
                    && AppliedComponentType is CompositeComponentType)
                {
                    HealthStatus = Logic.HealthStatus.SafeAndSound;
                }
            }
            return true;
        }

        public bool IsPlaceable(ComponentType componentType)
        {
            if (componentType == ComponentTypes.Phagocyte)
            {
                return Entity != null && Entity.ComponentType != ComponentTypes.Virus;
            }
            return Entity == null;
        }

        public void Update()
        {
            if (Entity != null && Entity.CanBeRemoved)
            {
                Reset();
            }
        }

        public void Reset()
        {
            Entity = null;
            AppliedComponentType = null;
        }

    }
}
