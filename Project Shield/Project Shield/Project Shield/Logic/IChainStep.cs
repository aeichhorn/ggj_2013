﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Project_Shield.Logic
{
    public interface IChainStep
    {
        ComponentType BestMatch { get; }
        IChainStep Apply(ComponentType componentType);
    }
}
