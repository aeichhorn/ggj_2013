﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Project_Shield.Logic
{
    public class NoChainStep : IChainStep
    {
        public ComponentType BestMatch { get { return null; } }

        public IChainStep Apply(ComponentType componentType)
        {
            return ChainStep.None;
        }
    }

    public class ChainStep : IChainStep
    {
        public static readonly IChainStep None = new NoChainStep();
        private readonly ComponentTypeContainer m_container;
        public ComponentType BestMatch { get; private set; }

        public ChainStep(ComponentType componentType)
        {
            m_container = new ComponentTypeContainer();
            m_container.ComponentTypes.Add(componentType, 1);
        }

        public ChainStep(IEnumerable<ComponentType> componentTypes)
        {
            m_container = new ComponentTypeContainer();
            foreach (var type in componentTypes)
            {
                m_container.Add(type);
            }
            CheckPossibleMatches();
        }

        private ChainStep(IDictionary<ComponentType, int> componentTypes, ComponentType componentType)
        {
            m_container = new ComponentTypeContainer(new Dictionary<ComponentType, int>(componentTypes));
            m_container.Add(componentType);
        }

        private bool CheckPossibleMatches()
        {
            BestMatch = ComponentTypes.AllComposites
                .Where(type => type.Matches(m_container.ComponentTypes))
                .OrderBy(type => type.Priority)
                .FirstOrDefault();
            return BestMatch != null;
        }

        public IChainStep Apply(ComponentType componentType)
        {
            var nextStep = new ChainStep(m_container.ComponentTypes, componentType);

            if (nextStep.CheckPossibleMatches())
            {
                return nextStep;
            }
            return ChainStep.None;
        }
    }



}
