using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Project_Shield.Logic;

namespace Project_Shield
{
	public class Game1 : Microsoft.Xna.Framework.Game
	{
		#region Variables
		// System
		GraphicsDeviceManager graphics;
		SpriteBatch spriteBatch;
		KeyboardState m_keyboardState;
		MouseState m_mouseState;
		SpriteFont m_arial24Font;
		Color m_spriteColor = new Color(252, 222, 215);

        //MainMenu
        private MainMenu m_mainMenu; 

        //Save/Load Highscore
        private Player m_playerData;

		// States and generals
		private enum GameState { Mainmenue, Playing, GameOver };
		private GameState m_gameState;
		private Random m_rand;

		// Field
        private Texture2D m_fieldTextureAtlas;
        private List<Rectangle> m_fieldTiles = new List<Rectangle>();
        private IDictionary<int, Rectangle> m_WoundTiles = new Dictionary<int, Rectangle>();
		private List<Field> m_fields = new List<Field>();

		private int m_rows = 4;
		private int m_columns = 10;
		private int m_tokenSize = 64;

		// Token
        private double m_timeSinceNoTokenPlaced = 0;
        private double m_countdown = 10; 

		private List<Token> m_tokenList = new List<Token>();

		// Interaction
		private Rectangle m_mouseRectangle;

		private Board m_board;
		private EventDispatcher m_eventDispatcher;

		// Heartbeat
		private Heartbeat m_heartbeat;
		private SoundEffect m_heartbeatEffect;
		public static bool PumpHeart = false;

		// Interface and background
		private Texture2D m_backgroundTexture;
		private Texture2D m_sidebarTexture;
		private Texture2D m_gameOverTexture;
        private Texture2D m_gameWonTexture;
		private Texture2D m_backButtonOver;
		private Rectangle m_gameOverRect = new Rectangle(397, 485, 649, 536);


        //Soundeffects
        private SoundEffect[] effect = new SoundEffect[12];
        private SoundEffectInstance[] soundEffectInstance = new SoundEffectInstance[12];
        private bool pickedup = false;
        Song background_song;

		#endregion


		public Game1()
		{
			graphics = new GraphicsDeviceManager(this);
			Content.RootDirectory = "Content";
		}

		protected override void LoadContent()
		{
			spriteBatch = new SpriteBatch(GraphicsDevice);
			m_arial24Font = Content.Load<SpriteFont>("Fonts/Arial");

			m_fieldTextureAtlas = Content.Load<Texture2D>("Background/Skin_sprites");

            Token.TokenTexture = Content.Load<Texture2D>("Token/creatures");
            var fieldTileSize = 64;
            for (int i = 0; i < 16; ++i)
            {
                int x = (i % 8);
                int y = (i / 8);
                m_fieldTiles.Add(new Rectangle(x * fieldTileSize, y * fieldTileSize, fieldTileSize, fieldTileSize));
            }

            m_WoundTiles.Add(0, new Rectangle(320, 128, fieldTileSize, fieldTileSize));
            m_WoundTiles.Add(127, new Rectangle(0 * fieldTileSize, 192, fieldTileSize, fieldTileSize));
            m_WoundTiles.Add(247, new Rectangle(1 * fieldTileSize, 192, fieldTileSize, fieldTileSize));
            m_WoundTiles.Add(253, new Rectangle(2 * fieldTileSize, 192, fieldTileSize, fieldTileSize));
            m_WoundTiles.Add(223, new Rectangle(3 * fieldTileSize, 192, fieldTileSize, fieldTileSize));
            m_WoundTiles.Add( 28, new Rectangle(4 * fieldTileSize, 192, fieldTileSize, fieldTileSize));
            m_WoundTiles.Add(124, new Rectangle(5 * fieldTileSize, 192, fieldTileSize, fieldTileSize));
            m_WoundTiles.Add(112, new Rectangle(6 * fieldTileSize, 192, fieldTileSize, fieldTileSize));
            m_WoundTiles.Add(31, new Rectangle(7 * fieldTileSize, 192, fieldTileSize, fieldTileSize));
            m_WoundTiles.Add(255, new Rectangle(8 * fieldTileSize, 192, fieldTileSize, fieldTileSize));
            m_WoundTiles.Add(241, new Rectangle(9 * fieldTileSize, 192, fieldTileSize, fieldTileSize));
            m_WoundTiles.Add(7, new Rectangle(10 * fieldTileSize, 192, fieldTileSize, fieldTileSize));
            m_WoundTiles.Add(199, new Rectangle(11 * fieldTileSize, 192, fieldTileSize, fieldTileSize));
            m_WoundTiles.Add(193, new Rectangle(12 * fieldTileSize, 192, fieldTileSize, fieldTileSize));
            

            var fieldNumber = m_rows * m_columns;
            for (int i = 0; i < fieldNumber; i++)
            {
                int x = (i % m_columns);
                int y = (i / m_columns);
                m_fields.Add(new Field(m_fieldTextureAtlas,
                                       new Vector2(64 + x * m_tokenSize, 128 + y * m_tokenSize),
                                       m_fieldTiles[i % 16],
                                       m_board.WoundAt(x, y),
                                       m_WoundTiles));
            }

			m_backgroundTexture = Content.Load<Texture2D>("Background/background");
			m_sidebarTexture = Content.Load<Texture2D>("UserInterface/Sidebar");
			m_gameOverTexture = Content.Load<Texture2D>("Background/Gameoverscreen");
            m_gameWonTexture = Content.Load<Texture2D>("Background/Winscreen");
			m_backButtonOver = Content.Load<Texture2D>("Buttons/back-mouseover");

            //Load Soundeffects
            background_song = Content.Load<Song>("Audio/mars-and-stars");
            MediaPlayer.IsRepeating = true;
            MediaPlayer.Play(background_song);

            effect[0] = Content.Load<SoundEffect>("Audio/blood");
            soundEffectInstance[0] = effect[0].CreateInstance();
            soundEffectInstance[0].IsLooped = false;
            effect[1] = Content.Load<SoundEffect>("Audio/merge");
            soundEffectInstance[1] = effect[1].CreateInstance();
            soundEffectInstance[1].IsLooped = false;
            effect[2] = Content.Load<SoundEffect>("Audio/eisen");
            soundEffectInstance[2] = effect[2].CreateInstance();
            soundEffectInstance[2].IsLooped = false;
            effect[3] = Content.Load<SoundEffect>("Audio/Eisweis");
            soundEffectInstance[3] = effect[3].CreateInstance();
            soundEffectInstance[3].IsLooped = false;
            effect[4] = Content.Load<SoundEffect>("Audio/fat");
            soundEffectInstance[4] = effect[4].CreateInstance();
            soundEffectInstance[4].IsLooped = false;
            effect[5] = Content.Load<SoundEffect>("Audio/fresszelle");
            soundEffectInstance[5] = effect[5].CreateInstance();
            soundEffectInstance[5].IsLooped = false;
            effect[6] = Content.Load<SoundEffect>("Audio/oxygen");
            soundEffectInstance[6] = effect[6].CreateInstance();
            soundEffectInstance[6].IsLooped = false;
            effect[7] = Content.Load<SoundEffect>("Audio/pickup_creature");
            soundEffectInstance[7] = effect[7].CreateInstance();
            soundEffectInstance[7].IsLooped = false;
            effect[8] = Content.Load<SoundEffect>("Audio/smack_virus");
            soundEffectInstance[8] = effect[8].CreateInstance();
            soundEffectInstance[8].IsLooped = false;
            effect[9] = Content.Load<SoundEffect>("Audio/sugar");
            soundEffectInstance[9] = effect[9].CreateInstance();
            soundEffectInstance[9].IsLooped = false;
            effect[10] = Content.Load<SoundEffect>("Audio/virus");
            soundEffectInstance[10] = effect[10].CreateInstance();
            soundEffectInstance[10].IsLooped = false;
            effect[11] = Content.Load<SoundEffect>("Audio/whitecell");
            soundEffectInstance[11] = effect[11].CreateInstance();
            soundEffectInstance[11].IsLooped = false;
		}

		protected override void Initialize()
		{
			Window.Title = "Cell Fusion";
			graphics.PreferredBackBufferWidth = 1024;
			graphics.PreferredBackBufferHeight = 576;
			graphics.ApplyChanges();
			IsMouseVisible = true;

			m_gameState = GameState.Mainmenue;

            m_mainMenu = new MainMenu(m_mouseState, new Vector2(m_mouseRectangle.X, m_mouseRectangle.Y), (int)m_gameState);
            m_mainMenu.LoadContent(Content);

            m_rand = new Random();
            m_playerData = new Player();
            m_board = BoardFactory.CreateWitHealthStatus(m_columns, m_rows);
            m_eventDispatcher = new EventDispatcher(m_board);
			m_playerData.LoadHighscore(0);

			m_heartbeatEffect = Content.Load<SoundEffect>("Audio/HeartbeatShort_2");
			m_heartbeat = new Heartbeat(m_heartbeatEffect);

			base.Initialize();
		}

		protected override void Update(GameTime gameTime)
		{
			m_keyboardState = Keyboard.GetState();
			m_mouseState = Mouse.GetState();
			if (m_keyboardState.IsKeyDown(Keys.Escape) || m_keyboardState.IsKeyDown(Keys.Q))
				Exit();
			m_mouseRectangle = new Rectangle(Mouse.GetState().X, Mouse.GetState().Y, 1, 1);


			switch (m_gameState)
			{
				case GameState.Mainmenue:
                    m_gameState = (GameState)m_mainMenu.UpdateMenu((int)m_gameState, spriteBatch, m_mouseState, new Vector2(m_mouseRectangle.X, m_mouseRectangle.Y));
					break;
				case GameState.Playing:
					m_eventDispatcher.Dispatch();

                    m_board.Update();
                    if (m_board.GameWon || m_board.GameOver)
                    {
                        m_gameState = GameState.GameOver;
                    }
					updateToken(gameTime);

                    m_playerData.Score = m_board.Score;

					grabToken();
					placeToken();
					
                    m_timeSinceNoTokenPlaced += gameTime.ElapsedGameTime.TotalSeconds;
                    m_countdown -= gameTime.ElapsedGameTime.TotalSeconds; 
                    if (m_countdown < 0)
                    {
                        m_countdown = 10; 
                    }
                    if (m_timeSinceNoTokenPlaced > 10f)
                    {
                        m_timeSinceNoTokenPlaced = 0;
                        var token = new Token(ComponentTypes.Virus);
                        Wound wound = m_board.SpawnVirus(token);
                        Field field = FindFieldForWound(wound);
                        if (field != null)
                        {
                            token.Position = new Vector2(field.GetFieldRectangle().X,
                                field.GetFieldRectangle().Y);
                            token.IsOnField = true;
                            m_tokenList.Add(token);
                            soundEffectInstance[10].Play();
                        }
                    }
					m_tokenList.RemoveAll((token) => token.CanBeRemoved);

					m_heartbeat.SetSoundAcceleration(20 * (m_playerData.Score / 100));
					m_heartbeat.Update(gameTime);
					break;
				case GameState.GameOver:
					// Reset and save score
					if (m_playerData.Score != 0)
						m_playerData.SaveHighscore((int)m_gameState);
					break;
				default:
                    Exit();
                    break;
			}

			base.Update(gameTime);
		}

        private Field FindFieldForWound(Wound wound)
        {
            foreach (var field in m_fields)
            {
                if (field.Represents(wound))
                {
                    return field;
                }
            }
            return null;
        }

		private void updateToken(GameTime gameTime)
		{
			// Spawn new token
			if (Game1.PumpHeart)
			{
				Game1.PumpHeart = false;
				foreach(Token token in m_tokenList)
					if (!token.IsOnField && !token.IsOnMouse)
						token.ActivateTokenImpulse();
				m_tokenList.Add(new Token(m_rand));
			}

			// Move Token
			foreach (Token token in m_tokenList)
			{
				token.Update(gameTime);
                if (token.ComponentType == ComponentTypes.Virus && token.EatenByHungryFellow)
                {
                    soundEffectInstance[8].Play();
                }
			}
		}

		private void grabToken()
		{
			for (int i = 0; i < m_tokenList.Count; i++)
			{
				if (m_tokenList[i].GetRectangle().Intersects(m_mouseRectangle)
					&& m_mouseState.LeftButton == ButtonState.Pressed
					&& !m_tokenList[i].IsOnField)
				{
					m_tokenList[i].IsOnMouse = true;
                    if (pickedup == false)
                    {
                        if (soundEffectInstance[7].State != SoundState.Playing)
                        {
                            soundEffectInstance[7].Play();
                        }
                        pickedup = true;
                    }
					break;
				}
			}
		}

		private void placeToken()
		{
            // Drop token
            for (int j = 0; j < m_tokenList.Count; j++)
            {
                Token token = m_tokenList[j];
                if (token.IsOnMouse && m_mouseState.LeftButton == ButtonState.Released)
                {
                    int intersectingFieldIndex = -1;
                    for (int k = 0; k < m_fields.Count; k++)
                    {
                        if (m_fields[k].GetFieldState() >= HealthStatus.Injured
                            && m_fields[k].IsPlaceable(token.ComponentType)
                            && m_mouseRectangle.Intersects(m_fields[k].GetFieldRectangle())
                            )
                        {
                            intersectingFieldIndex = k;
                            break;
                        }
                    }

                    if (intersectingFieldIndex > -1)
                    {
                        m_timeSinceNoTokenPlaced = 0;
                        m_countdown = 10; 
                        Field field = m_fields[intersectingFieldIndex];
                        int x = (intersectingFieldIndex % m_columns);
                        int y = (intersectingFieldIndex / m_columns);
                        token.Position = new Vector2(field.GetFieldRectangle().X,
                            field.GetFieldRectangle().Y);
                        token.IsOnField = true;
                        IEnumerable<Match> matches = m_board.ApplyComponent(x, y, token);
                        foreach (Match match in matches)
                        {
                            m_eventDispatcher.Add(new MergeGameEvent(match, token, soundEffectInstance[1]));
                        }
                    }
                    else
                    {
                        token.GoToPosition = new Vector2(token.Position.X, 510);
                    }
                    switch((int)m_tokenList[j].Type){
                        case 2:                     
                            if (soundEffectInstance[9].State != SoundState.Playing)
                            {
                                soundEffectInstance[9].Play();
                            }
                            break;
                        case 3:      //Lipide
                            if (soundEffectInstance[4].State != SoundState.Playing)
                            {
                                soundEffectInstance[4].Play();
                            }
                            break;
                        case 4:      //Oxygen
                            if (soundEffectInstance[6].State != SoundState.Playing)
                            {
                                soundEffectInstance[6].Play();
                            }
                            break;
                        case 5:      //Iron
                            if (soundEffectInstance[2].State != SoundState.Playing)
                            {
                                soundEffectInstance[2].Play();
                            }
                            break;
                        case 6:      //Proteine  
                            if (soundEffectInstance[3].State != SoundState.Playing)
                            {
                                soundEffectInstance[3].Play();
                            }
                            break;
                        case 7:      //Blood
                            if (soundEffectInstance[0].State != SoundState.Playing)
                            {
                                soundEffectInstance[0].Play();
                            }
                            break;
                        case 8:      //Phagocyte
                            if (soundEffectInstance[5].State != SoundState.Playing)
                            {
                                soundEffectInstance[5].Play();
                            }
                           break;
                    }
                    
                    token.IsOnMouse = false;
                    pickedup = false;
                    break;
                }
            }
		}

		protected override void Draw(GameTime gameTime)
		{
			GraphicsDevice.Clear(Color.Chocolate);
            spriteBatch.Begin();

            switch (m_gameState)
            {
                case GameState.Mainmenue:
                    m_mainMenu.DrawMenu(spriteBatch);
                    break;
                case GameState.Playing:
					spriteBatch.Draw(m_backgroundTexture, Vector2.Zero, Color.White);

			        foreach (Field field in m_fields)
				        field.Draw(spriteBatch);

			        foreach (Token token in m_tokenList)
				        token.Draw(spriteBatch);


					spriteBatch.Draw(m_sidebarTexture, Vector2.Zero, Color.White);
					// Draw score and highscore
					if ( m_playerData.Score > 0)
						spriteBatch.DrawString(m_arial24Font, m_playerData.Score.ToString() + "k", new Vector2(139, 11), m_spriteColor);
					else
						spriteBatch.DrawString(m_arial24Font, m_playerData.Score.ToString(), new Vector2(139, 11), m_spriteColor);
					if (m_playerData.GetHighscore() > 0)
						spriteBatch.DrawString(m_arial24Font, m_playerData.GetHighscore().ToString() + "k", new Vector2(503, 11), m_spriteColor);
					else
						spriteBatch.DrawString(m_arial24Font, m_playerData.GetHighscore().ToString(), new Vector2(503, 11), m_spriteColor);

                    int time = (int)m_countdown;
                    spriteBatch.DrawString(m_arial24Font, time.ToString(), new Vector2(720, 395), Color.Brown);

                    break;
                case GameState.GameOver:
                    if (m_board.GameOver)
                    {
                        spriteBatch.Draw(m_gameOverTexture, Vector2.Zero, Color.White);
						spriteBatch.DrawString(m_arial24Font, m_playerData.m_highscore.ToString() + "k", new Vector2(600, 330), m_spriteColor);
						spriteBatch.DrawString(m_arial24Font, m_playerData.Score.ToString() + "k", new Vector2(600, 380), m_spriteColor);
                    }
                    if (m_board.GameWon)
                    {
						spriteBatch.Draw(m_gameWonTexture, Vector2.Zero, Color.White);
						spriteBatch.DrawString(m_arial24Font, m_playerData.m_highscore.ToString() + "k", new Vector2(600, 330), m_spriteColor);
						spriteBatch.DrawString(m_arial24Font, m_playerData.Score.ToString() + "k", new Vector2(600, 380), m_spriteColor);
                    }
                    break;
            }


			spriteBatch.End();
			base.Draw(gameTime);
		}
	}
}
