﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Runtime.InteropServices;

namespace Project_Shield
{
    class Player
    {
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern uint MessageBox(IntPtr hWnd, String text, String caption, uint type);

        public int Score = 0; 

        public int m_highscore = 0;

        XmlTextWriter writer;
        XmlReader reader;

        public int SaveHighscore(int GState)
        {
            try
            {
                if (m_highscore > Score)
                {
                    return GState;
                }

                writer = new XmlTextWriter("Content/SaveScore.xml", null);
                writer.WriteStartDocument();
                writer.WriteStartElement("mainnode");

                writer.WriteStartElement("highscore");
                writer.WriteValue(Score);

                writer.WriteEndDocument();
                writer.Flush();
                writer.Close();
            }
            catch (System.NullReferenceException)
            {
                MessageBox(new IntPtr(0), "An Error occured!", "Null Reference Exception", 0);
                return 3;
            }
            catch (System.UnauthorizedAccessException)
            {
                MessageBox(new IntPtr(0), "Couldn't get access to the file! Can't save the game!", "Unauthorized Access Exception", 0);
                return 3;
            }
            catch (System.Xml.XmlException)
            {
                writer.Close();
                MessageBox(new IntPtr(0), "Couldn't write the file!", "XML Exception", 0);
                return 3;
            }

            return GState;
        }

        public int LoadHighscore(int GState)
        {
            try
            {
                reader = XmlReader.Create("Content/SaveScore.xml");
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element && reader.Name == "highscore")
                         m_highscore = reader.ReadElementContentAsInt();
                }
                reader.Close();
            }
            catch (System.Xml.XmlException)
            {
                reader.Close();
            }
            catch (System.NullReferenceException)
            {
                MessageBox(new IntPtr(0), "An Error occured! Back to Menu!", "Null Reference Exception", 0);
                return 3;
            }
            catch (System.UnauthorizedAccessException)
            {
                MessageBox(new IntPtr(0), "Couldn't get access to the file! Can't save the game!", "Unauthorized Access Exception", 0);
            }
            return GState;
      }

		public int GetHighscore()
		{
			return m_highscore;
		}
    }
}
