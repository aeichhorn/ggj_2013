﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Project_Shield
{
    class MainMenu
    {
        private MouseState mouseState, mouseoldState;
        private int MState = 0, MoldState = 0;
        private enum GameState {Mainmenue, Playing, GameOver};
        private GameState m_gameState;
        private bool m_credits = false;
        private Texture2D[] menuNavi = new Texture2D[6];

        public Vector2 mousePos;
        public SpriteFont font;
        public SoundEffect[] effect = new SoundEffect[2];
        public SoundEffectInstance[] soundEffectInstance = new SoundEffectInstance[2];

		private Texture2D m_creditsOverlay, m_creditsText;
		private Vector2 m_creditsTextPosition = new Vector2(335, 380);

        public MainMenu(MouseState Mousestate, Vector2 MousePos, int gameState)
        {
            m_gameState = (GameState)gameState;
            mouseState = Mousestate;
            mousePos = MousePos;
        }

        public bool MouseCollision(long targetx, long targety, long targetx1, long targety1)
        {
            bool missed = (mousePos.X > targetx && mousePos.X < targetx1 &&
            mousePos.Y > targety && mousePos.Y < targety1);
            return missed;
        }

        public void LoadContent(ContentManager myContent)
        {
            menuNavi[0] = myContent.Load<Texture2D>("Buttons/new-mouseover");
            menuNavi[1] = myContent.Load<Texture2D>("Buttons/end-mouseover");
            menuNavi[2] = myContent.Load<Texture2D>("Buttons/credits-mouseover");
            menuNavi[3] = myContent.Load<Texture2D>("Buttons/back-mouseover");
            menuNavi[4] = myContent.Load<Texture2D>("Background/Startscreen");
            menuNavi[5] = myContent.Load<Texture2D>("Background/Creditsscreen");
            font = myContent.Load<SpriteFont>("Fonts/Arial");

			effect[0] = myContent.Load<SoundEffect>("Audio/menu_overmouse");
            soundEffectInstance[0] = effect[0].CreateInstance();
            soundEffectInstance[0].IsLooped = false;
            effect[1] = myContent.Load<SoundEffect>("Audio/menu_click");
            soundEffectInstance[1] = effect[1].CreateInstance();
            soundEffectInstance[1].IsLooped = false;

			m_creditsOverlay = myContent.Load<Texture2D>("Background/Creditsscreen-rahmen");
			m_creditsText = myContent.Load<Texture2D>("Background/Credits Text");
        }

        public int UpdateMenu(int GState, SpriteBatch spriteBatch, MouseState mymouseState, Vector2 mymousePos)
        {
            mousePos = mymousePos;
            mouseState = mymouseState;

            if (!m_credits)
            {
                if ((mouseState.LeftButton == ButtonState.Pressed && mouseoldState.LeftButton != ButtonState.Pressed &&
                    (MouseCollision(386, 225, 649, 276) || MouseCollision(386, 288, 649, 339) || MouseCollision(386, 485, 649, 536))))
                {
                    if (soundEffectInstance[1].State != SoundState.Playing)
                    {
                        soundEffectInstance[1].Play();
                    }
                    switch (MState)
                    {
                        case 0:
                            m_gameState = GameState.Playing;
                            break;
                        case 1:
                            m_credits = true;
                            break;
                        case 2:
                        default:
                            m_gameState = (GameState)3;
                            break;
                    }
                }
                if (MouseCollision(386, 225, 649, 276))
                {
                    MState = 0;
                    if (soundEffectInstance[0].State == SoundState.Stopped && MoldState != MState)
                    {
                        soundEffectInstance[0].Play();
                    }
                }
                else if (MouseCollision(386, 288, 649, 339))
                {
                    MState = 1;
                    if (soundEffectInstance[0].State == SoundState.Stopped && MoldState != MState)
                    {
                        soundEffectInstance[0].Play();
                    }
                }
                else if (MouseCollision(386, 485, 649, 536))
                {
                    MState = 2;
                    if (soundEffectInstance[0].State == SoundState.Stopped && MoldState != MState)
                    {
                        soundEffectInstance[0].Play();
                    }
                }
            }
            else
            {
                if ((mouseState.LeftButton == ButtonState.Pressed && mouseoldState.LeftButton != ButtonState.Pressed && MouseCollision(397, 485, 649, 536)))
                {
                    m_credits = false;
                    if (soundEffectInstance[1].State == SoundState.Stopped)
                    {
                        soundEffectInstance[1].Play();
                    }

                }
            }
			if ( m_creditsTextPosition.Y > -500)
			m_creditsTextPosition.Y -= 0.75f;
            mouseoldState = mouseState;
            MoldState = MState; 
            return (int)m_gameState;
        }

        public void DrawMenu(SpriteBatch spriteBatch)
        {
            //Check Mouseposition and draw Menu
            if (m_credits == false)
            {
                spriteBatch.Draw(menuNavi[4], new Vector2(0, 0), Color.White);
                switch (MState)
                {
                    case 0:
						spriteBatch.Draw(menuNavi[0], new Vector2(385, 228), Color.White);
                        break;
                    case 1:
						spriteBatch.Draw(menuNavi[2], new Vector2(385, 291), Color.White);
                        break;
                    case 2:
						spriteBatch.Draw(menuNavi[1], new Vector2(385, 488), Color.White);
                        break;
                    default:
                        break;
                }
            }
            else
            {

				spriteBatch.Draw(menuNavi[5], new Vector2(0, 0), Color.White);
				spriteBatch.Draw(m_creditsText, m_creditsTextPosition, Color.White);
				spriteBatch.Draw(m_creditsOverlay, Vector2.Zero, Color.White);

				if (MouseCollision(386, 485, 649, 536))
                {
                    MState = 3; 
					spriteBatch.Draw(menuNavi[3], new Vector2(385, 488), Color.White);
                    if (soundEffectInstance[0].State == SoundState.Stopped && MoldState != MState)
                    {
                        soundEffectInstance[0].Play();
                    }
                }
                else
                {
                    MState = 2; 
                }
            }
        }
    }
}
