﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Project_Shield.Logic;

namespace Project_Shield
{
    class EventDispatcher
    {
        private readonly Queue<GameEvent> m_eventQueue = new Queue<GameEvent>();

        private Board m_board;
        public EventDispatcher(Board board)
        {
            m_board = board;
        }

        public void Add(GameEvent gameEvent)
        {
            m_eventQueue.Enqueue(gameEvent);
        }

        public void Dispatch()
        {
            while (m_eventQueue.Count > 0)
            {
                dynamic gameEvent = m_eventQueue.Dequeue();
                HandleEvent(gameEvent);
            }
        }

        public void HandleEvent(GameEvent gameEvent)
        {
        }

        public void HandleEvent(MergeGameEvent gameEvent)
        {
            Match match = gameEvent.Match;
            IEnumerable<Token> tokens = match.Wounds
                .Take(2)
                .Select(wound => wound.Entity as Token);
            foreach (var token in tokens)
            {
                token.GoToPosition = gameEvent.Token.Position;
            
            }
            if (gameEvent.Token.ComponentType != match.ComponentType)
            {
                gameEvent.Token.ApplyCompositeType(match.ComponentType);
                var wound = match.Wounds.Last();
                m_board.ApplyComponent(wound.X, wound.Y, gameEvent.Token);
                gameEvent.mergeSound.Play();
            }
        }
    }
}
