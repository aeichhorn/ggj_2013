﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Project_Shield.Logic;

namespace Project_Shield
{
	class Field
	{
		#region Variables
        private readonly Vector2 m_position;
		private readonly Texture2D m_texture;
        private readonly Rectangle m_frame;
        private readonly IDictionary<int, Rectangle> m_woundFrames;
        private readonly Rectangle m_fieldRect;
        private readonly Wound m_wound;
		#endregion

		public Field(Texture2D texture, Vector2 position, Rectangle frame, Wound wound, IDictionary<int, Rectangle> woundFrames)
		{
			m_texture = texture;
			m_position = position;
            m_frame = frame;
            m_woundFrames = woundFrames;
            m_wound = wound;
            m_fieldRect = new Rectangle((int)m_position.X, (int)m_position.Y, m_frame.Width, m_frame.Height);
		}

        public bool IsPlaceable(ComponentType componentType)
        {
            return m_wound.IsPlaceable(componentType);
        }


		public void Draw(SpriteBatch spriteBatch)
		{
			spriteBatch.Draw(m_texture, m_position, m_frame, Color.White);
            if (m_woundFrames.ContainsKey(m_wound.NeighborState) && m_wound.HealthStatus > HealthStatus.SafeAndSound)
            {
                spriteBatch.Draw(m_texture, m_position, m_woundFrames[m_wound.NeighborState], Color.White);
            }
		}

        public bool Represents(Wound wound)
        {
            return wound == m_wound;
        }

		/// <summary>
		/// State 0 == SafeAndSound, 1 = Wounded, 2 == Severly Injured
		/// </summary>
		/// <returns>State</returns>
		public HealthStatus GetFieldState()
		{
			return m_wound.HealthStatus;
		}

		public Rectangle GetFieldRectangle()
		{
            return m_fieldRect;
		}
	}
}
