﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Project_Shield.Logic;

namespace Project_Shield
{
	class Token : IToken
	{
		#region Variables

		public static Texture2D TokenTexture;
        private MobileSprites TokenSprite; 
		//public static IList<MobileSprites> TokenSprites = new List<MobileSprites>();

        public Project_Shield.Logic.Type Type { get { return ComponentType.Type; } }
		public ComponentType ComponentType { get; private set; }

		public Vector2 Position;

		public bool IsOnField { get; set; }
		public bool IsOnMouse { get; set; }
        public bool EatenByHungryFellow { get; set; }

		private int m_maxSpeed = 25;
		private int m_currentSpeed = 25;
		private bool m_pumpTokenImpulse = false;
		private float m_alpha = 1;

 		public bool CanBeRemoved 
        { 
            get 
            { 
                return Position.X <= -64 || m_removeDueToMerge || EatenByHungryFellow; 
            } 
        }
		
        public Vector2 GoToPosition;
		private bool m_removeDueToMerge;
		#endregion

		public Token(Random rand)
        {
            int type = rand.Next(0, 8);
            if (rand.Next(0, 1000) < 5)
            {
                ComponentType = ComponentTypes.Virus;
            }
            else
            {
                ComponentType = ComponentTypes.AllBasicComponents[type];
            }

			Position = new Vector2(770, 468 + rand.Next(0, 41));
			GoToPosition = Vector2.Zero;

            Initialize();
		}

        public Token(ComponentType type)
        {
            ComponentType = type;

            Initialize();
        }

        private void Initialize()
        {
            TokenSprite = new MobileSprites(TokenTexture);
            InitializeMobileSprites();

            //Loop Path
            TokenSprite.LoopPath = false;

            //Move on a special path?
            TokenSprite.IsPathing = false;

            //If true, Sprite moves to target
            TokenSprite.IsMoving = false;
        }        

		public void Update(GameTime gameTime)
		{

			if (Type == Logic.Type.Cell)
			{
				m_alpha -= 0.001f;
				if ( m_alpha <= 0)
					m_removeDueToMerge = true;
			}
            TokenSprite.Update(gameTime);

			if (GoToPosition != Vector2.Zero)
			{
				var direction = GoToPosition - Position;
				if (direction.Length() < 25.0f)
				{
					GoToPosition = Vector2.Zero;
					if (Position.Y < 450)
						m_removeDueToMerge = true;
					return;
				}
				direction.Normalize();
				Position += direction * (float)(1 * gameTime.ElapsedGameTime.TotalMilliseconds);
				return;
			}
			else if (!IsOnField && !IsOnMouse)
			{
				Position.X -= m_currentSpeed;
				if ( m_currentSpeed > 1)
					Position.Y += (int)(Math.Sin(5 * Position.X) * 2);

				m_currentSpeed--;
				m_currentSpeed = (int)MathHelper.Max(m_currentSpeed, 1);
				if (m_pumpTokenImpulse)
				{
					m_currentSpeed = m_maxSpeed;
					m_pumpTokenImpulse = false;
				}
			}
			else if (IsOnMouse)
			{
				MouseState mouseState = Mouse.GetState();
				Position = new Vector2(mouseState.X - 32, mouseState.Y - 32);
			}
		}

		public void ApplyCompositeType(ComponentType type)
		{
			ComponentType = type;
            InitializeMobileSprites();
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            //spriteBatch.Draw(TokenTextures[(int)Type - 1], Position, Color.White);
			
			TokenSprite.Position = Position;
			TokenSprite.Draw(spriteBatch, m_alpha);
        }

        public Rectangle GetRectangle()
        {
            return new Rectangle((int)Position.X, (int)Position.Y, 64, 64);
        }

		public void ActivateTokenImpulse()
		{
			m_pumpTokenImpulse = true;
		}

        public void InitializeMobileSprites()
        {
            switch ((int)Type)
            {
                case 1:      // Virus
                    TokenSprite.Sprite.AddAnimation("virus", 192, 0, 64, 64, 3, 0.2f);
                    //Set current Animation
                    TokenSprite.Sprite.CurrentAnimation = "virus";
                    break;
                case 2:      //Sugar
                    TokenSprite.Sprite.AddAnimation("sugar", 384, 0, 64, 64, 3, 0.2f);
                    //Set current Animation
                    TokenSprite.Sprite.CurrentAnimation = "sugar";
                    break;
                case 3:      //Lipide
                    TokenSprite.Sprite.AddAnimation("lipide", 0, 192, 64, 64, 3, 0.1f);
                    //Set current Animation
                    TokenSprite.Sprite.CurrentAnimation = "lipide";
                    break;
                case 4:      //Oxygen
                    TokenSprite.Sprite.AddAnimation("oxygen", 0, 128, 64, 64, 3, 0.2f);
                    //Set current Animation
                    TokenSprite.Sprite.CurrentAnimation = "oxygen";
                    break;
                case 5:      //Iron
                    TokenSprite.Sprite.AddAnimation("iron", 0, 0, 64, 64, 3, 0.2f);
                    //Set current Animation
                    TokenSprite.Sprite.CurrentAnimation = "iron";
                    break;
                case 6:      //Proteine  
                    TokenSprite.Sprite.AddAnimation("proteine", 384, 64, 64, 64, 3, 0.2f);
                    //Set current Animation
                    TokenSprite.Sprite.CurrentAnimation = "proteine";
                    break;
                case 7:      //Blood
                    TokenSprite.Sprite.AddAnimation("blood", 192, 128, 64, 64, 3, 0.2f);
                    //Set current Animation
                    TokenSprite.Sprite.CurrentAnimation = "blood";
                    break;
                case 8:      //Phagocyte
                    TokenSprite.Sprite.AddAnimation("phagocyte", 0, 64, 64, 64, 3, 0.2f);
                    //Set current Animation
                    TokenSprite.Sprite.CurrentAnimation = "phagocyte";
                    break;
                case 9:      //Thrombocyte
                    TokenSprite.Sprite.AddAnimation("thrombocyte", 192, 192, 64, 64, 3, 0.1f);
                    //Set current Animation
                    TokenSprite.Sprite.CurrentAnimation = "thrombocyte";
                    break;
                case 10:     //Leucocyte
                    TokenSprite.Sprite.AddAnimation("leucocyte", 384, 128, 64, 64, 3, 0.2f);
                    //Set current Animation
                    TokenSprite.Sprite.CurrentAnimation = "leucocyte";
                    break;
                case 11:     //Cell
                    TokenSprite.Sprite.AddAnimation("cell", 192, 64, 64, 64, 3, 0.2f);
                    //Set current Animation
                    TokenSprite.Sprite.CurrentAnimation = "cell";
                    break;
            }
        }
	}
}
