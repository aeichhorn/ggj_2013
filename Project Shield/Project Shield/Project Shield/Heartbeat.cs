﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Project_Shield.Logic;

namespace Project_Shield
{
	class Heartbeat
	{
		private SoundEffect m_heartbeat;

		private int m_acceleration = 0;
		private KeyboardState m_keyboardState;

		private int m_defaultWaveTime = 1000;
		private int m_timeBetweenWaves = 1000;
		private int m_timeBetweenWaveElapsed = 0;


		public Heartbeat(SoundEffect heartbeat)
		{
			m_heartbeat = heartbeat;
		}

		public void Update(GameTime gameTime)
		{
			m_timeBetweenWaveElapsed += (int)gameTime.ElapsedGameTime.TotalMilliseconds;
			if (m_timeBetweenWaveElapsed > m_timeBetweenWaves)
			{
				m_timeBetweenWaveElapsed = 0;
				m_heartbeat.Play();
				Game1.PumpHeart = true;
			}

			m_keyboardState = Keyboard.GetState();

			

			//if (m_keyboardState.IsKeyDown(Keys.I))
			//    m_acceleration -= 5;
			//if (m_keyboardState.IsKeyDown(Keys.O))
			//    m_acceleration += 5;

			m_acceleration = (int)MathHelper.Clamp(m_acceleration, 0, 980);
			m_timeBetweenWaves = m_defaultWaveTime - m_acceleration;
		}

		/// <summary>
		/// Change the time that is passing between each wave.
		/// </summary>
		/// <param name="value">The amount that will be subtracted from the spawn-wave time. Maximal "980" ms</param>
		public void SetSoundAcceleration(int value)
		{
			m_acceleration = value;
		}
	}
}
