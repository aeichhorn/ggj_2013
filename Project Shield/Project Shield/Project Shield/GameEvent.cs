﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Project_Shield.Logic;
using Microsoft.Xna.Framework.Audio;

namespace Project_Shield
{
    enum EventType
    {
        MergeComponent,
        MergeAnimationStart,
        MergeAnimationEnd
    }
    
    class GameEvent
    {
        public EventType Type { get; private set;}
        
        public GameEvent(EventType type)
        {
            Type = type;
        }
    }


    class MergeGameEvent : GameEvent
    {
        public Match Match { get; private set; }
        public Token Token { get; private set; }
        public SoundEffectInstance mergeSound; 

        public MergeGameEvent(Match match, Token token, SoundEffectInstance MergeSound)
            :base(EventType.MergeComponent)
        {
            Match = match;
            Token = token;
            mergeSound = MergeSound; 
        }
    }
    
}
